package com.ljs.alipaytest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.ljs.alipaytest.mapper")//浏览接口的包
public class AlipaytestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlipaytestApplication.class, args);
    }

}
