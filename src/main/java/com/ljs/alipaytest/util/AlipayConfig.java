package com.ljs.alipaytest.util;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String APP_ID = "2016102600765358";

    // 商户私钥，您的PKCS8格式RSA2私钥，这些就是我们刚才设置的
    public static String APP_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDHcRqaLW2dMktNu1TlEk6sAIEQBG/2sLzJ9Ckg9BI82gsEJnJfk7lyNiHOpqV21xuJ9HVhqqGkSbHwUKGduKaU9ekQ4KOHBKRBJ30EIhfUP2qXY3hkL/c9R8UW6X2zNb/8H0n650KHAXanRBkPhTKq8TUAv+EDED+5lnKqh1MwgM8P5iym4PfLd74vr+o9p9jiu1FfFcyokwNYWhY3hYz5bLnxKAgZ2QYd6yxcrEvFmqOTYaeiLSn5IYmCvC4wV15WK5xrFOvsw6HelTG87RI6PbD6gZo13P4D2+xSFaVTy/6BL2WqVdSQwDMg6W5PIcgVy5MmvoMHcYKT98RcVrixAgMBAAECggEAQOVnv5VanpT39eIn/KXZXZSIanrXJ9FYjOpa27MvoAdfIEmWW81IwYYluAV3v9S8zEX0UARitn+YW9cQz65+3eeqn+hIra1v+RdtUYP6dwDvoTUnvonxDEqnMg5l7YNm5hYUT4Uim6V5Q3IgKXGxC/iSw5ysDgAijDQNObnw8s3T14YakDja3UVf6Gbju2Yvwcfp60eDOD47HJvNX71YhPHGyPA3bflSJfxfRhu1yAetlV5nFeWnt7YwDDd997eu4HC3Al0DwKcclVeAXk3gg3s/ivQmzR5mgA5656043nU+HBa+ZG1H3XMmtwcvDaJMJ2fAqnOWZ+4EqyYOddleMQKBgQDta504dABBMtxU3pnRQgb6vWekxtdNba3mzMydQIIoy1RVxmRhTv17xOnwio6KXVypnq2/cF0P0m8iwuDPLV8HSfFPjGZHy5pKGOfdloqq94SgaGtaqa0Eyb8L4U56s6NAbUyopslC0Acn36olOfRbO52ulONYR4kAhlGwyFG5BQKBgQDXDKRz3V0FUkTLoDYvPHRmiA6v1WozOCP2u3bhQVBKTnTUpHSuJiTsckGl0P//SFDsp3PnkRXRaQxFd7PHB5urL+kbbwCVcJYN8yx/dAQJZERRnyPBprQNv+GcKwFVraOaijGUUOAO9wgnI72rfML+gjvp2W/h/rlDK7Y3KUagvQKBgQDPXsFR/7ABQoVD87HyHnOuKiUOvUZssLHF93d4mlGVhBNDYCdo/u9UXaffm4XowKcg2CH2JhSCr1C20fQ9wOlRmnoU+xKS/vjiY2KSHt07eznySYILDEy88+HUZJSBg8nBijX80MDVhJHbd/ZK0ZXm4elWBWGVR9YYCTns6OrIaQKBgFBgYksIDs+BW0M8Y2QuVVdJPSwNOT9hmVeL47f3gHn1bwdXXGEG0sApSE4gmr4gmlaJk/GWMbBuce64+dHHDzDhnF/rki1Iwlz+acBNbYDRIBPdxjCIDT4iXMHQ9RO/7alCQ5uqysPtIvf2DNs+q2cS6C/UXFP8r+PiP53IaKSFAoGBAJu6Wp2FswiqDv4f6TyOxa5BXQ+E1HwsDq1T+Wn2LpaomX7OBgJ5FhKRoANMvJkBulE5kngd1PBfB+CAQIUG3pECk6uHGo+zktfbsHlP8JIGCM5Kj1stKwmTabd0R692CWSWZz45o04OV+WUh+JgIz1I9diuId6Nv3w0d7RHll8q";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm
    // 对应APPID下的支付宝公钥。，这些就是我们刚才设置的
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhpp5DJW09tEXJqyHh4siCCTJj85pIcNuzBbX2MQx9pvEPEtzC5gxe1ZZlGAp5JOGCzrFnQWLAxe0d6nIPkjormNpE3li0fgPnNBF3pmMHzA2z8XmqrDS5iJ9CEYYVxC6qiPQSezFZMgA4xFDJg+MycziItpxxwQQ+Dsos9imHlDskY6yFc+gEzTcehOOjfpHbRhvmQqJXhUBZf9Ju70ld/GFwcHE8EaFW37L84BTGVr+XHQdxoTqqka9pQvAk2B3DPec+5h3PxAu8hjzznjtQF4u/mLSQp+yAkRwZRwSSrSZF/rbw0g+ACJEQiWl0/cZEu7rbxvryGAupoUJeZXDWQIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://134.175.219.59:7777/notifyPayResult";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问(其实就是支付成功后返回的页面)
    public static String return_url = "http://134.175.219.59:7777//alipay/callback/return";//这里可以回调出订单id

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String CHARSET = "utf-8";

    // 支付宝网关，这是沙箱的网关
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "/root";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
