package com.ljs.alipaytest.service.impl;

import com.ljs.alipaytest.bean.Order;

import java.util.List;

public interface Orderservices {
    /**
     * 添加订单
     * @param order
     * @return
     */
    public  boolean insertOrder(Order order);

    /**
     * 查询所以订单
     * @return
     */
    public List<Order> selorder();
}
