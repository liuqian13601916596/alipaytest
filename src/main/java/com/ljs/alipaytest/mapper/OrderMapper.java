package com.ljs.alipaytest.mapper;

import com.ljs.alipaytest.bean.Order;

import java.util.List;

public interface OrderMapper  {
    /**
     * 添加订单
     * @param order
     * @return
     */
    public  boolean insertOrder(Order order);

    /**
     * 查询所以订单
     * @return
     */
    public List<Order> selorder();

}
