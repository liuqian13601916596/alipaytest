package com.ljs.alipaytest.bean;

import lombok.Data;

import java.math.BigDecimal;
@Data
/*订单*/
public class Order {
    private Integer orderId;
    private String orderNo;
    private String userId;
    private BigDecimal orderAmount;
    private Integer orderStatus;
    private String createTime;
    private String lastUpdateTime;

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", orderNo='" + orderNo + '\'' +
                ", userId='" + userId + '\'' +
                ", orderAmount=" + orderAmount +
                ", orderStatus=" + orderStatus +
                ", createTime='" + createTime + '\'' +
                ", lastUpdateTime='" + lastUpdateTime + '\'' +
                '}';
    }
}
